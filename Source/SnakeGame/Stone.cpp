// Fill out your copyright notice in the Description page of Project Settings.


#include "Stone.h"

#include "DrawDebugHelpers.h"
#include "SnakeBase.h"
#include "SPlayerState.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AStone::AStone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AStone::BeginPlay()
{
	Super::BeginPlay();

	GenerateField();

	Respawn();
}

void AStone::GenerateField()
{
	AllLocations.Empty();
	FVector L_Location = FVector::ZeroVector;
	constexpr float FieldSize = 16.f;
	constexpr float L_FieldCenter = (FieldSize/1.6f * 120.f) + 100.f;
	FieldCenter = L_FieldCenter;
	for(int i = 0; i <= FieldSize; i++)
	{
		FieldX = i * 100.f - L_FieldCenter;
		for(int j = 0; j <= FieldSize; j++)
		{
			FieldY = j * 100.f - L_FieldCenter;
			L_Location = UKismetMathLibrary::MakeVector(FieldX, FieldY, 0.f);
			AllLocations.Add(L_Location);
		}
	}
}

void AStone::TakeLive(ASnakeBase* SnakeBase)
{
	if(ASPlayerState* PlayerState = SnakeBase->GetInstigator()->GetPlayerState<ASPlayerState>())
	{
		PlayerState->TakeLive(1);
	}
}

// Called every frame
void AStone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AStone::Interact(AActor* Interactor, bool bIsHead)
{
	
	if (bIsHead)
	{
		const auto Snake = Cast<ASnakeBase>(Interactor);
		
		if (IsValid(Snake))
		{
			TakeLive(Snake);
			Snake->SetActorTickInterval(Snake->MovementSpeed = 0.5f);
			Snake->Live -= 1;
			if(Snake->Live<=0)
			{
				Snake->Destroyed();
				Snake->Destroy();
			}
			Respawn();
			CameraShakeDemo(0.1f);
		}
	}
}

FVector AStone::TraceCheckedLocation()
{
	bool bTraceHit;
	FVector NewLocation;
	
	do
	{
		NewLocation = AllLocations[FMath::RandRange(0, AllLocations.Num()-1)];
		FHitResult TraceHitResult;
		FVector TraceStart = NewLocation - FVector(0.0f, 0.0f, 50.0f);
		FVector TraceEnd = NewLocation + FVector(0.0f, 0.0f, 200.0f);
	
		bTraceHit = GetWorld()->LineTraceSingleByChannel(TraceHitResult, TraceStart, TraceEnd, ECC_Visibility);
		DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Yellow, false,
			3.f, 0, 10.f);
		if(bTraceHit)
		{
			DrawDebugSphere(GetWorld(), TraceHitResult.GetActor()->GetActorLocation(),
				50.f, 24, FColor::Green, false, 5.0f);
			SetActorLocation(NewLocation);
		}
		bTraceHit = TraceHitResult.bBlockingHit;
	}
	while (bTraceHit);
	
	return NewLocation;
}

void AStone::Respawn()
{
	SetActorLocation(TraceCheckedLocation());
}

void AStone::CameraShakeDemo(float Scale) const
{
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->StartCameraShake(SCameraShake, Scale);
}


