// Fill out your copyright notice in the Description page of Project Settings.


#include "SPlayerState.h"
#include "SSaveGame.h"

int32 ASPlayerState::GetPlayerLives() const
{
	return Lives;
}

void ASPlayerState::TakeLive(int32 Delta)
{
	Lives -= Delta;
	OnLivesChanged.Broadcast(this, Lives, Delta);
}

int32 ASPlayerState::GetPlayerScore() const
{
	return PlayerScore;
}

int32 ASPlayerState::GetHighScore() const
{
	return HighScore;
}

void ASPlayerState::AddScore(int32 Delta)
{
	PlayerScore += Delta;
	OnScoreChanged.Broadcast(this, PlayerScore, Delta);
}

void ASPlayerState::LoadPlayerState_Implementation(USSaveGame* SaveObject)
{
	if(SaveObject)
	{
		HighScore = SaveObject->HighScore;
	}
}

void ASPlayerState::SavePlayerState_Implementation(USSaveGame* SaveObject)
{
	if(SaveObject)
	{
		if(PlayerScore > HighScore)
		{
			SaveObject->HighScore = PlayerScore;
		}
		else
		{
			SaveObject->HighScore = HighScore;
		}
	}
}