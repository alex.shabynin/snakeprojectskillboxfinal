// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "SPlayerState.h"

// Sets default values
AWall::AWall()
{
	PrimaryActorTick.bCanEverTick = true;
	
}

void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWall::TakeLive(ASnakeBase* SnakeBase)
{
	if(ASPlayerState* PlayerState = SnakeBase->GetInstigator()->GetPlayerState<ASPlayerState>())
	{
		PlayerState->TakeLive(1);
	}
}

void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AWall::Interact(AActor* Interactor, bool bIsHead)
{
	IInteractable::Interact(Interactor, bIsHead);

	const auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		/*if (const APlayerPawnBase* PlayerPawnBase = Interactor->GetInstigator()->GetController()->GetPawn<APlayerPawnBase>())
		{
			//PlayerPawnBase->PlayPortalSound(GetActorLocation());
		}*/
				
		if (!Snake->InPortal)
		{
			Snake->InPortal = true;
			TakeLive(Snake);
			Snake->SetActorTickInterval(Snake->MovementSpeed = 0.5f);
			Snake->Live -= 1;
			if(Snake->Live<=0)
			{
				Snake->Destroyed();
				Snake->Destroy();
			}

			FVector MovementVector(FVector::ZeroVector);
			
			switch (Snake->LastMoveDirection)
			{
			case EMovementDirection::UP:
				MovementVector.X -= PortalDistance /2;
				break;
			case EMovementDirection::DOWN:
				MovementVector.X += PortalDistance /2;
				break;
			case EMovementDirection::LEFT:
				MovementVector.Y -= PortalDistance /2;
				break;
			case EMovementDirection::RIGHT:
				MovementVector.Y += PortalDistance /2;
				break;
			}
			Snake->SnakeElements[0]->AddActorWorldOffset(MovementVector);
			Snake->InPortal = false;

		}
	}
}

