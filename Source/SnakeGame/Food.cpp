// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"

#include "SnakeBase.h"
#include "DrawDebugHelpers.h"
#include "SPlayerState.h"
#include "Kismet/KismetMathLibrary.h"
#include "PlayerPawnBase.h"
#include "GameFramework/Pawn.h"

AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AFood::BeginPlay()
{
	Super::BeginPlay();
	GenerateField();
	RespawnFood();
}


void AFood::GenerateField()
{
	AllLocations.Empty();
	FVector L_Location = FVector::ZeroVector;
	constexpr float FieldSize = 16.f;
	constexpr float L_FieldCenter = (FieldSize/1.6f * 120.f) + 100.f;
	FieldCenter = L_FieldCenter;
	for(int i = 0; i <= FieldSize; i++)
	{
		FieldX = i * 100.f - L_FieldCenter;
		for(int j = 0; j <= FieldSize; j++)
		{
			FieldY = j * 100.f - L_FieldCenter;
			L_Location = UKismetMathLibrary::MakeVector(FieldX, FieldY, 0.f);
			AllLocations.Add(L_Location);
		}
	}
	
}

void AFood::EatFood(ASnakeBase* SnakeBase)
{
	if(ASPlayerState* PlayerState = SnakeBase->GetInstigator()->GetPlayerState<ASPlayerState>())
	{
		PlayerState->AddScore(1);
		SnakeBase->PlayerPawnBase->AddSeconds();
	}
}

void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		if (ASnakeBase* SnakeBase = Cast<ASnakeBase>(Interactor))
		{
			EatFood(SnakeBase);
			SnakeBase->AddSnakeElement();
			SnakeBase->SetActorTickInterval(SnakeBase->MovementSpeed -= 0.05f);
			if(SnakeBase->MovementSpeed <= 0.15f)
			SnakeBase->SetMovementSpeed(0.15f);
			RespawnFood();
		}
	}
}




FVector AFood::TraceCheckedLocation()
{
	bool bTraceHit;
	FVector NewLocation;
	
	do
	{
		NewLocation = AllLocations[FMath::RandRange(0, AllLocations.Num()-1)]; //FVector(GetRandomLocation(), 0.0f);
		FHitResult TraceHitResult;
		FVector TraceStart = NewLocation - FVector(0.0f, 0.0f, 50.0f);
		FVector TraceEnd = NewLocation + FVector(0.0f, 0.0f, 200.0f);
	
		bTraceHit = GetWorld()->LineTraceSingleByChannel(TraceHitResult, TraceStart, TraceEnd, ECC_Visibility);
		DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Yellow, false,
			3.f, 0, 10.f);
		if(bTraceHit)
		{
			DrawDebugSphere(GetWorld(), TraceHitResult.GetActor()->GetActorLocation(),
				50.f, 24, FColor::Green, false, 5.0f);
			SetActorLocation(NewLocation);
		}
		bTraceHit = TraceHitResult.bBlockingHit;
	}
	while (bTraceHit);
	
	return NewLocation;
}

void AFood::RespawnFood()
{
	SetActorLocation(TraceCheckedLocation());
}
