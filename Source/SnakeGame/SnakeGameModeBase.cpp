// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeGameModeBase.h"
#include "SPlayerState.h"
#include "SSaveGame.h"
#include "GameFramework/GameStateBase.h"
#include "Kismet/GameplayStatics.h"


ASnakeGameModeBase::ASnakeGameModeBase()
{
	SlotName = "HighScore";
}

void ASnakeGameModeBase::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options,ErrorMessage);

	LoadSaveGame();
}

void ASnakeGameModeBase::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);
	
	ASPlayerState* PS = NewPlayer->GetPlayerState<ASPlayerState>();
	if(PS)
	{
		PS->LoadPlayerState(CurrentSaveGame);
	}
}

void ASnakeGameModeBase::WriteSaveGame()
{
	ASPlayerState* PS = Cast<ASPlayerState>(GameState->PlayerArray[0]);
	if(PS)
	{
		PS->SavePlayerState(CurrentSaveGame);
	}
	
	UGameplayStatics::SaveGameToSlot(CurrentSaveGame, SlotName, 0);
}

void ASnakeGameModeBase::LoadSaveGame()
{
	if (UGameplayStatics::DoesSaveGameExist(SlotName, 0))
	{
		CurrentSaveGame = Cast<USSaveGame>(UGameplayStatics::LoadGameFromSlot(SlotName, 0));
		if(CurrentSaveGame == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("Filed To Load SaveGame Data."))
			return;
		}
		UE_LOG(LogTemp, Warning, TEXT("Loaded SaveGame Data."))
	}
	else
	{
		CurrentSaveGame = Cast<USSaveGame>(UGameplayStatics::CreateSaveGameObject(USSaveGame::StaticClass()));

		UE_LOG(LogTemp, Warning, TEXT("Create new SaveGame Data."))
	}
}