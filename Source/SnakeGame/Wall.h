// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Wall.generated.h"

class ASnakeBase;

UCLASS()
class SNAKEGAME_API AWall : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void TakeLive(ASnakeBase* SnakeBase);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

protected:
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float PortalDistance = 1900;
	
};
