// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerPawnBase.h"
#include "SnakeGameModeBase.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ASnakeBase();
	
	bool bCanTurn;

	bool InPortal = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Live = 3;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize = 100.f;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase>SnakeElementClass;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY()
		ASnakeElementBase* PrevElement = nullptr;

	UPROPERTY()
		APlayerPawnBase* PlayerPawnBase;
	
	UPROPERTY()
		TArray<ASnakeElementBase*>SnakeElements;
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	
	virtual void Destroyed() override;

	ASnakeGameModeBase* GetGameMode() const;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	UFUNCTION()
		void SetMovementSpeed(float SpeedValue);
	
};
