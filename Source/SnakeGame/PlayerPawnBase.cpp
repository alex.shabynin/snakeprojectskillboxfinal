// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"


APlayerPawnBase::APlayerPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
	const FName CurrentLevelName = FName(UGameplayStatics::GetCurrentLevelName(GetWorld()));
	if (CurrentLevelName != FName("MainMenu"))
	{
		SetActorRotation(FRotator(-70,180,0));
		CreateSnakeActor();
	}

	FInputModeGameOnly InputModeData;
	InputModeData.SetConsumeCaptureMouseDown(true);
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	PlayerController->SetInputMode(InputModeData);

	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle,this, &APlayerPawnBase::HungerTimer,1.f,true,0.0f);
}

void APlayerPawnBase::CreateGameOverWidget()
{
	GameOverWidget = CreateWidget<UGameOverWidget>(GetWorld(),GameOverWidgetClass);
	if(GameOverWidget)
	GameOverWidget->Setup();
	UGameplayStatics::SetGamePaused(GetWorld(),true);
}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Instigator = this;
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform(), SpawnParameters);
}


void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if(IsValid(SnakeActor) && (SnakeActor->bCanTurn == false))
	{
		if(value>0 && SnakeActor->LastMoveDirection!=EMovementDirection::UP)
		{
			SnakeActor->bCanTurn = true;
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
		else if (value<0 && SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->bCanTurn = true;
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
	}
	
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if(IsValid(SnakeActor) && SnakeActor->bCanTurn == false)
	{
		if(value>0 && SnakeActor->LastMoveDirection!=EMovementDirection::RIGHT)
		{
			SnakeActor->bCanTurn = true;
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
		else if (value<0 && SnakeActor->LastMoveDirection!=EMovementDirection::LEFT)
		{
			SnakeActor->bCanTurn = true;
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
	}
}

void APlayerPawnBase::HungerTimer()
{
	
	if(Seconds !=0)
	{
		Seconds -=1;
	}
	else
	{
		if(Minutes == 0)
		{
			CreateGameOverWidget();
			SnakeActor->Destroyed();
			SnakeActor->Destroy();
		}
		else
		{
			Minutes -=1;
			Seconds = 59;
		}
	}
}

int APlayerPawnBase::AddSeconds()
{

	return Seconds +=SecondsToAdd;
}
