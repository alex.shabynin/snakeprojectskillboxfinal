// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameModeBase.generated.h"

class USSaveGame;

UCLASS()
class SNAKEGAME_API ASnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
protected:

	UPROPERTY()
		FString SlotName;
	
	UPROPERTY()
		USSaveGame* CurrentSaveGame;
	
public:
	
	UPROPERTY(BlueprintReadWrite)
		bool IsMenu = true;

	ASnakeGameModeBase();
	
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;

	virtual void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;

	UFUNCTION(BlueprintCallable, Category="SaveGame")
		void WriteSaveGame();
	
	UFUNCTION(BlueprintCallable, Category="SaveGame")
		void LoadSaveGame();
	
};
