// Fill out your copyright notice in the Description page of Project Settings.


#include "GameOverWidget.h"
#include "SnakeGameModeBase.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

void UGameOverWidget::Setup()
{
	this->AddToViewport();

	GetGameMode()->WriteSaveGame();

	const UWorld* World = GetWorld();

	PlayerController = World->GetFirstPlayerController();

	FInputModeGameAndUI InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::LockOnCapture);
	InputModeData.SetHideCursorDuringCapture(false);

	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = true;

}

void UGameOverWidget::RestartButtonClicked()
{
	const FName CurrentLevelName = FName(UGameplayStatics::GetCurrentLevelName(GetWorld()));
	UGameplayStatics::OpenLevel(GetWorld(), CurrentLevelName, true);
}

void UGameOverWidget::ExitButtonClicked()
{
	UKismetSystemLibrary::QuitGame(GetWorld(), PlayerController, EQuitPreference::Quit, true);
}

ASnakeGameModeBase* UGameOverWidget::GetGameMode()
{
	ASnakeGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASnakeGameModeBase>();
	
	return GameMode;
}

void UGameOverWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (RestartButton && ExitButton)
	{
		RestartButton->OnClicked.AddDynamic(this, &UGameOverWidget::RestartButtonClicked);
		ExitButton->OnClicked.AddDynamic(this, &UGameOverWidget::ExitButtonClicked);
	}
}