// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeBase.h"
#include "Food.generated.h"

class APlayerPawnBase;

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	AFood();
	
protected:
	virtual void BeginPlay() override;

	void GenerateField();
	
	void EatFood(ASnakeBase* SnakeBase);

	TArray<FVector> AllLocations;

	float FieldX;
	float FieldY;
	float FieldCenter;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
	

	UFUNCTION()
	FVector TraceCheckedLocation();

	UFUNCTION()
	void RespawnFood();

	

		
};
