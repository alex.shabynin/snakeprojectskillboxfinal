// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "Camera/CameraShakeBase.h"
#include "GameOverWidget.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	APlayerPawnBase();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateGameOverWidget();

	void CreateSnakeActor();
	

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	void HungerTimer();

	int AddSeconds();

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Timer")
	int Minutes = 0;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Timer")
	int Seconds = 20;
	
protected:
	UPROPERTY(BlueprintReadWrite)
		UGameOverWidget* GameOverWidget;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<UGameOverWidget> GameOverWidgetClass;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase>SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
	int SecondsToAdd = 2;
};
